<?php

namespace App\Repository;

use App\Entity\Prices;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Prices|null find($id, $lockMode = null, $lockVersion = null)
 * @method Prices|null findOneBy(array $criteria, array $orderBy = null)
 * @method Prices[]    findAll()
 * @method Prices[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PricesRepository extends ServiceEntityRepository {
	public function __construct(RegistryInterface $registry) {
		parent::__construct($registry, Prices::class);
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public function findActualProductPrices($id) {
		return $this->createQueryBuilder('p')
		            ->andWhere('p.product = :id')
		            ->andWhere('p.date_to >= CURRENT_TIMESTAMP()')
		            ->andWhere('p.is_default <> 1')
		            ->setParameter('id', $id)
		            ->orderBy('p.id')
		            ->getQuery()
		            ->getResult();
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public function findTodaysActualPrices($id) {
		return $this->createQueryBuilder('p')
		            ->andWhere('p.product = :id')
		            ->andWhere('CURRENT_TIMESTAMP() between p.date_from and p.date_to')
		            ->andWhere('p.is_default <> 1')
		            ->setParameter('id', $id)
		            ->orderBy('p.id')
		            ->getQuery()
		            ->getResult();
	}
}
