<?php

namespace App\DataFixtures;

use App\Entity\Prices;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Validator\Constraints\DateTime;

class AppFixtures extends Fixture {
	public function load(ObjectManager $manager) {

		for ($i = 0; $i < 120; $i ++) {
			$product = new Product();
			$product->setName('product' . $i);
			$def          = true;
			$prices_count = mt_rand(1, 5);
			for ($j = 0; $j < $prices_count; $j ++) {
				$price = new Prices();
				$price->setValue(mt_rand(10, 5000));
				$price->setIsDefault($def);

				$def       = false;
				$date_from = new \DateTime();
				$date_to   = clone $date_from;

				$price->setDateFrom($date_from->modify('+ ' . mt_rand(1, 10) . 'days'));
				$price->setDateTo($date_to->modify('+ ' . mt_rand(2, 1000) . 'days'));
				$price->setUpdated(new \DateTime());

				$manager->persist($price);

				$product->addPrice($price);
			}
			$manager->persist($product);
		}
		$manager->flush();
	}
}