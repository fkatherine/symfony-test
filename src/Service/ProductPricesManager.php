<?php
/**
 * Created by PhpStorm.
 * User: kathy
 * Date: 08.07.18
 * Time: 11:10
 */

namespace App\Service;


class ProductPricesManager {

	/**
	 * Sort product Prices
	 *
	 * @param $prices
	 * @param string $sort_criteria
	 *
	 * @return array
	 */
	public function getSortedPrices($prices, $sort_criteria = 'default'): array {
		switch ($sort_criteria) {
			case 'default':
				usort($prices, function ($price1, $price2) {
					$interval1 = $price1->getDateInterval()->days !== false ? $price1->getDateInterval()->days : $price1->getDateInterval()->y * DAYS_IN_YEAR;
					$interval2 = $price2->getDateInterval()->days !== false ? $price2->getDateInterval()->days : $price1->getDateInterval()->y * DAYS_IN_YEAR;
					if ($interval1 > $interval2) {
						return 1;
					} else if ($interval1 < $interval2) {
						return - 1;
					} else if ($interval1 == $interval2) {
						return 0;
					}
				});
				break;
			case 'date':
				usort($prices, function ($price1, $price2) {
					if ($price1->getUpdated() > $price2->getUpdated()) {
						return - 1;
					} else if ($price1->getUpdated() < $price2->getUpdated()) {
						return 1;
					} else if ($price1->getUpdated() == $price2->getUpdated()) {
						return 0;
					}
				});
				break;

		}

		return $prices;
	}

	/**
	 * Transform PricesObjects to Arrays
	 *
	 * @param $priceObjects
	 *
	 * @return array
	 */
	public function buildPricesArray($priceObjects): array {
		$prices = array();
		while ($priceObjects) {
			$price    = array_shift($priceObjects);
			$prices[] = array(
				'id'       => $price->getId(),
				'updated'  => $price->getUpdated(),
				'value'    => $price->getValue(),
				'dateFrom' => $price->getDateFrom() ? $price->getDateFrom()->format('d.m.Y') : '-',
				'dateTo'   => $price->getDateTo() ? $price->getDateTo()->format('d.m.Y') : '-',
				'default'  => $price->getIsDefault()
			);
		};

		return $prices;
	}

	/**
	 * @param $dates
	 *
	 * @return string
	 */
	public function getSortCriteria($dates) {
		$sortBy = 'default';
		$today  = new \DateTime();

		if (in_array($today->format('d.m.Y'), $dates)) {
			$sortBy = 'date';
		}

		return $sortBy;
	}

}