<?php
/**
 * Created by PhpStorm.
 * User: kathy
 * Date: 07.07.18
 * Time: 11:10
 */

namespace App\Controller;

use App\Entity\Prices;
use App\Entity\Product;
use App\Service\ProductPricesManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductController extends AbstractController {

	/**
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function index() {

		$products         = array();
		$productObjects   = $this->getDoctrine()->getRepository(Product::class)->findAll();
		$pricesRepository = $this->getDoctrine()->getRepository(Prices::class);
		$productManager   = new ProductPricesManager();

		foreach ($productObjects as $pObj) {
			$criteria    = $productManager->getSortCriteria($pObj->getSpecialDates());
			$prices      = $productManager->getSortedPrices($pricesRepository->findTodaysActualPrices($pObj->getId()), $criteria);
			$actualPrice = $prices ? reset($prices)->getValue() : false;

			$products[] = array(
				'id'           => $pObj->getId(),
				'name'         => ucfirst($pObj->getName()),
				'defaultPrice' => $pObj->getDefaultPrice()->getValue(),
				'actualPrice'  => $actualPrice,
				'prices'       => $productManager->buildPricesArray($prices)
			);
		}

		return $this->render('index/index.html.twig', array(
			'h1'       => 'Welcome to Prices Manager!',
			'products' => $products
		));
	}


	/**
	 * /product/{id}
	 *
	 * @param $id
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function show($id) {
		$productRepository = $this->getDoctrine()->getRepository(Product::class);
		$productManager    = new ProductPricesManager();

		$product = $productRepository->find($id);

		return $this->render('product/product_details.html.twig', array(
			'h1'      => ucfirst($product->getName()),
			'product' => array(
				'id'           => $product->getId(),
				'defaultPrice' => $product->getDefaultPrice()->getValue(),
				'prices'       => $productManager->buildPricesArray($product->getPrices()->getValues()),
				'specialDates' => $product->getSpecialDates()
			)
		));
	}
}