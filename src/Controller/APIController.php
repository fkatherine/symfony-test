<?php
/**
 * Created by PhpStorm.
 * User: kathy
 * Date: 08.07.18
 * Time: 15:09
 */

namespace App\Controller;

use App\Entity\Prices;
use App\Entity\Product;
use DateTime;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class APIController extends FOSRestController {

	/**
	 *
	 * @param Request $request
	 *
	 * @return string
	 */
	public function index(Request $request) {
		$data          = $request->request->all();
		$entityManager = $this->getDoctrine()->getManager();

		if ( ! isset($data['productId'])) {
			$productId  = explode('-', $data['id']);
			$data['id'] = next($productId);
			$productId  = reset($productId);
		}
		if (isset($data['id']) and $data['id'] != "") {
			$price = $this->getDoctrine()->getRepository(Prices::class)->find($data['id']);
		} else {
			$price = new Prices();
		}
		$result = new Response();
		switch ($data['action']) {
			case 'edit':
				$result = $data['id'] ? $this->update($data, $price, $entityManager) : $this->create($productId, $data, $price, $entityManager);
				break;
			case 'delete':
				$result = $this->delete($price, $entityManager);
				break;
			case 'setDates':
				return $this->setDates($data, $entityManager);
				break;
		}

		return new Response($result);
	}

	public function update($data, $price, $entityManager) {

		$dateFrom = $data['dateFrom'] != '-' ? DateTime::createFromFormat('d.m.Y', $data['dateFrom']) : null;
		$dateTo   = $data['dateFrom'] != '-' ? DateTime::createFromFormat('d.m.Y', $data['dateTo']) : null;

		$price->setValue($data['price']);

		if ($this->checkDates($dateFrom, $dateTo) and !$price->getIsDefault()) {
			$price->setDateFrom($dateFrom)
			      ->setDateTo($dateTo);
			$entityManager->persist($price);
			$entityManager->flush();
		}else {
			exit();
		}

		if ($price->getIsDefault()) {
			if ($dateFrom) {
				$price->setDateFrom($dateFrom);
				if ($dateTo and $this->checkDates($dateFrom, $dateTo)) {
					$price->setDateTo($dateTo);
				} else {
					exit();
				}
			}
			$entityManager->persist($price);
			$entityManager->flush();
		}


		return json_encode($data);
	}

	public function create($productId, $data, $price, $entityManager) {
		$product = $this->getDoctrine()->getRepository(Product::class)->find($productId);

		$dateFrom = DateTime::createFromFormat('d.m.Y', $data['dateFrom']);
		$dateTo   = DateTime::createFromFormat('d.m.Y', $data['dateTo']);

		if ($this->checkDates($dateFrom, $dateTo)) {
			$price->setValue($data['price'])
			      ->setDateFrom($dateFrom)
			      ->setDateTo($dateTo)
			      ->setIsDefault(false)
			      ->setProduct($product)
			      ->setUpdated(new DateTime());

			$entityManager->persist($price);
			$entityManager->flush();
		}

		return json_encode($data);
	}

	public function delete($price, $entityManager) {
		$entityManager->remove($price);
		$entityManager->flush();

		return json_encode('done');
	}

	public function setDates($data, $entityManager) {
		$product  = $this->getDoctrine()->getRepository(Product::class)->find($data['productId']);
		$datesArr = explode(', ', $data['specDates']);
		$product->setSpecialDates($datesArr);
		$entityManager->persist($product);
		$entityManager->flush();

		return $this->redirect('/product/' . $data['productId'], 301);
	}

	public function checkDates($dFrom, $dTo) {
		if($dFrom > $dTo) {
			return false;
		}
		return true;
	}

}