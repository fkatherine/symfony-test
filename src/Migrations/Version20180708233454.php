<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180708233454 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE special_dates (id INT AUTO_INCREMENT NOT NULL, date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE special_dates_product (special_dates_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_79F683CBBD14F954 (special_dates_id), INDEX IDX_79F683CB4584665A (product_id), PRIMARY KEY(special_dates_id, product_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE special_dates_product ADD CONSTRAINT FK_79F683CBBD14F954 FOREIGN KEY (special_dates_id) REFERENCES special_dates (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE special_dates_product ADD CONSTRAINT FK_79F683CB4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE special_dates_product DROP FOREIGN KEY FK_79F683CBBD14F954');
        $this->addSql('DROP TABLE special_dates');
        $this->addSql('DROP TABLE special_dates_product');
    }
}
