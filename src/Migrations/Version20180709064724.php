<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180709064724 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE prices CHANGE date_from date_from DATETIME DEFAULT CURRENT_TIMESTAMP, CHANGE date_to date_to DATETIME DEFAULT CURRENT_TIMESTAMP');
	    $this->addSql('ALTER TABLE product CHANGE special_dates special_dates LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

	    $this->addSql('ALTER TABLE prices CHANGE date_from date_from DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, CHANGE date_to date_to DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
	    $this->addSql('ALTER TABLE product CHANGE special_dates special_dates LONGTEXT NOT NULL');
    }
}
