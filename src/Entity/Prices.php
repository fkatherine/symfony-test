<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PricesRepository")
 */
class Prices {
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="prices", fetch="EAGER")
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
	 */
	private $product;

	/**
	 * @ORM\Column(type="float")
	 */
	private $value;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $is_default;

	/**
	 * @ORM\Column(type="datetime",options={"default":"CURRENT_TIMESTAMP"})
	 */
	private $date_from;

	/**
	 * @ORM\Column(type="datetime",options={"default":"CURRENT_TIMESTAMP"})
	 */
	private $date_to;

	/**
	 * @ORM\Column(type="datetime",options={"default":"CURRENT_TIMESTAMP"})
	 */
	private $updated;

	public function getId() {
		return $this->id;
	}

	public function getProduct(): ?Product {
		return $this->product;
	}

	public function setProduct(?Product $product): self {
		$this->product = $product;

		return $this;
	}

	public function getValue(): ?float {
		return $this->value;
	}

	public function setValue(float $value): self {
		$this->value = $value;

		return $this;
	}

	public function getIsDefault(): ?bool {
		return $this->is_default;
	}

	public function setIsDefault(bool $is_default): self {
		$this->is_default = $is_default;

		return $this;
	}

	public function getDateFrom(): ?\DateTimeInterface {
		return $this->date_from;
	}

	public function setDateFrom(\DateTimeInterface $date_from): self {
		$this->date_from = $date_from;

		return $this;
	}

	public function getDateTo(): ?\DateTimeInterface {
		return $this->date_to;
	}

	public function setDateTo(\DateTimeInterface $date_to): self {
		$this->date_to = $date_to;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getUpdated() {
		return $this->updated;
	}

	/**
	 * @param mixed $updated
	 */
	public function setUpdated($updated): void {
		$this->updated = $updated;
	}

	public function getDateInterval() {
		if ( ! $this->is_default) {
			return $this->date_to->diff($this->date_from);
		} else {
			// set big date interval for default prices
			return new \DateInterval('P99999Y');
		}

	}
}
