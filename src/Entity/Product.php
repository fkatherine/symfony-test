<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

define('DAYS_IN_YEAR', 365);

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product {
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Prices", mappedBy="product", fetch="EAGER")
	 */
	private $prices;

	/**
	 * @ORM\Column(type="text")
	 */
	private $specialDates;

	public function __construct() {
		$this->prices       = new ArrayCollection();
		$this->specialDates = new ArrayCollection();
	}

	public function getId() {
		return $this->id;
	}

	public function getName(): ?string {
		return $this->name;
	}

	public function setName(string $name): self {
		$this->name = $name;

		return $this;
	}

	/**
	 * @return Collection|Prices[]
	 */
	public function getPrices(): Collection {
		return $this->prices;
	}

	/**
	 * @return Collection|Prices[]
	 */
	public function getDefaultPrice() {
		foreach ($this->prices as $price) {
			if ($price->getIsDefault()) {
				return $price;
			}
		}
	}

	public function addPrice(Prices $price): self {
		if ( ! $this->prices->contains($price)) {
			$this->prices[] = $price;
			$price->setProduct($this);
		}

		return $this;
	}

	public function removePrice(Prices $price): self {
		if ($this->prices->contains($price)) {
			$this->prices->removeElement($price);
			// set the owning side to null (unless already changed)
			if ($price->getProduct() === $this) {
				$price->setProduct(null);
			}
		}

		return $this;
	}

	public function getSpecialDates() {
		$dates = array();
		$today = strtotime(date('d.m.Y'));
		if ($this->specialDates) {
			$spDates = unserialize($this->specialDates);
			foreach ($spDates as $key => $special_date) {
				$date = strtotime($special_date);
				if ($date >= $today) {
					$dates[] = $special_date;
				}
			}
		}

		return $dates;
	}

	public function setSpecialDates(array $dates): self {
		$this->specialDates = serialize($dates);

		return $this;
	}
}
