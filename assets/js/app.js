$(document).ready(function () {
    $('#editable-table').Tabledit({
        url: '/api',
        editmethod: 'post',
        rowIdentifier: 'data-id',
        columns: {
            identifier: [0, 'id'],
            editable: [[1, 'price'], [2, 'dateFrom'], [3, 'dateTo']]
        },
        restoreButton: false,
        onAlways: function(){
            location.reload();
        }
    });

    $('input[name="dateFrom"]').datepicker({
            format: 'dd.mm.yyyy',
            minDate: new Date(),
        }
    );
    $('input[name="dateTo"]').datepicker({
            format: 'dd.mm.yyyy',
            minDate: new Date(),
        }
    );

    var $multiplePicker = $('#multidatepicker'),
        datepickerValues = makeValues();

    // console.log(datepickerValues);

    $multiplePicker.datepicker({
            format: 'dd.mm.yyyy',
            multipleDates: true,
            minDate: new Date(),
        }
    );

    $multiplePicker.data('datepicker').selectDate(datepickerValues);

    $("#addRow").click(function (e) {
        e.preventDefault();
        var tableditTableName = '#editable-table';
        var newID = $(tableditTableName + " tr:last").attr("data-id");
        var clone = $("#editable-table tr:last").clone();
        $(".tabledit-view-mode span", clone).text("");
        $(".tabledit-view-mode input", clone).val("");
        clone.appendTo(tableditTableName);
        $(tableditTableName + " tr:last").attr("data-id", newID.split('-')[0] + '-');
        $(tableditTableName + " tr:last td .tabledit-span.tabledit-identifier").text('');
        $(tableditTableName + " tr:last td .tabledit-input.tabledit-identifier").val(newID.split('-')[0] + '-');
        $(tableditTableName + " tr:last td .badge").text('');
        $('.datepicker').datepicker('destroy');
        $('input[name^="date"]').datepicker({
                format: 'dd.mm.yyyy',
                minDate: new Date(),
            }
        );
    });

    function makeValues() {
        var dateStr = $('#multidatepicker').val().split(', '),
            res = [];
        for (i = 0; i < dateStr.length; i++) {
            if (dateStr[i].length != 0) {
                dmY = dateStr[i].split('.');
                res.push(new Date(dmY[2], dmY[1]-1, dmY[0]));
            }
        }
        return res;
    }

});